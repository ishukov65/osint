import fitz
import re
import os

def search_emails_in_pdf(pdf_file):
    emails = set()
    
    try:
        pdf_document = fitz.open(pdf_file)

        for page_num in range(pdf_document.page_count):
            page = pdf_document[page_num]
            text = page.get_text()
            
            # Регулярное выражение для поиска электронных почт
            email_pattern = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
            emails.update(re.findall(email_pattern, text))

        pdf_document.close()
    except fitz.FileDataError as e:
        print(f"Error processing the file {pdf_file}: {e}")

    return emails

def search_emails_in_directory(directory):
    emails = set()

    # Перебор все файлов в директории
    for filename in os.listdir(directory):
        if filename.endswith(".pdf"):
            file_path = os.path.join(directory, filename)
            emails.update(search_emails_in_pdf(file_path))

    return emails

pdf_directory = "/home/kali/week1"

found_emails = search_emails_in_directory(pdf_directory)

output_file = "found_emails.txt"

# Запись найденных эл. адресов в текстовый файл
with open(output_file, "w") as file:
    for email in found_emails:
        file.write(email + "\n")

print(f"The found emails are saved in the file: {output_file}")
