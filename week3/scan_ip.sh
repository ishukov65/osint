!/bin/bash

# List of IP addresses to scan
IP_ADDRESSES=(
"91.144.179.162"
"91.144.179.168"
"91.144.179.165"
"91.144.179.164"
"91.144.179.170"
"94.181.180.78"
"91.144.179.169"
"83.234.199.28"
"83.234.199.70"
"83.234.199.21"
"83.234.199.15"
"83.234.199.5"
"83.234.199.6"
"217.150.37.125"
"91.217.21.20"
"91.217.20.20"
"91.189.116.13"
"91.189.116.16"
"109.233.174.7"
"109.233.174.40"
"109.233.174.11"
)

# Function to scan the IP address and save the results
scan_ip() {
    scan_ip() {
    ip=$1
    nmap -T2 -sV -O -p 0-65535 --min-parallelism=300 --max-retries=0 --min-rate=100000 --host-timeout=15m -Pn -oX - $ip  >> nmap_scan_result>
    echo "--------------------------------" >> nmap_scan_results.txt
}
 
# Cleaning the file before scanning
> nmap_scan_results.txt

# Parallel scanning of each IP address
for ip in "${IP_ADDRESSES[@]}"
do
    scan_ip $ip
done
